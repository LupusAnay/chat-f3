<?php

date_default_timezone_set('Europe/Moscow');

$f3=require('lib/base.php');

$f3->config('api/config.ini');
$f3->config('api/routes.ini');


$db=new DB\SQL
(
	$f3->get('db_type').':host='.$f3->get('db_host').';port='.$f3->get('db_port').';dbname='.$f3->get('db_name'),
	$f3->get('db_login'),
	$f3->get('db_password')
);

$f3->set('db', $db);

$_smtp= array( 
	"host" => "smtp.yandex.ru", 	// smtp сервер 
	"debug" => 0, 				// отображение информации дебаггера (0 - нет вообще) 
	"SMTPAuth" => true, 		// authentication enabled 
	"SMTPSecure" => 'tls', 		// secure transfer enabled REQUIRED for Gmail 
	"auth" => true, 			// сервер требует авторизации 
	"port" => 587, 				// порт (по-умолчанию - 25) // or 587 
	"username" => $f3->get('ma'),			// имя пользователя на сервере 
	"password" => $f3->get('pass'),			// пароль 
	"addreply" => $f3->get('ma'),			// ваш е-mail 
	"replyto" =>  $f3->get('ma')			// e-mail ответа 
);

$f3->run();
