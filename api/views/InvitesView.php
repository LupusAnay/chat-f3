<?php namespace views;

use Base;

use models\chat\ChatsModel;
use models\errors\DatabaseError;
use models\errors\EntryAlreadyExists;
use models\errors\EntryNotFound;
use models\errors\ValidationError;
use models\invite\Invite;
use models\invite\InvitesModel;

use views\responses\EmptyResponse;
use views\responses\ForbiddenError;
use views\responses\NotFoundError;
use views\responses\NotImplementedError;
use views\responses\ResourceConflictError;
use views\responses\Response;
use views\responses\ServiceUnavailableError;
use views\responses\UnprocessableEntityError;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 18:11
 */
class InvitesView
{
    public static function new_invite(Base $app): Response
    {
        $username = $app->get('SESSION.username');
        if (!$username) return new ForbiddenError();
        $chats_model = new ChatsModel($app->get('db'));

        try {
            $invite = new Invite(json_decode($app->get('BODY')));
            $chat_id = $invite->get_chat_id();

            if (!$chats_model->user_is_admin($chat_id, $username))
                return new ForbiddenError();

            $invites_model = new InvitesModel($app->get('db'));
            $id = $invites_model->create_invite($invite);

        } catch (ValidationError $e) {
            return new UnprocessableEntityError($e->getMessage());
        } catch (DatabaseError $e) {
            return new ServiceUnavailableError($e->getMessage());
        } catch (EntryAlreadyExists $e) {
            return new ResourceConflictError($e->getMessage());
        }

        return new Response(200, array('id' => $id));
    }

    public static function invite_status(Base $app, array $params): Response
    {
        return new NotImplementedError();
    }

    public static function delete_invite(Base $app, array $params): Response
    {
        return new NotImplementedError();
    }

    public static function accept(Base $app, array $params): Response
    {
        $username = $app->get('SESSION.username');
        if (!$username) return new ForbiddenError();

        $invite_id = $params['invite_id'];
        $model = new InvitesModel($app->get('db'));

        try {
            $invite = $model->get_by_id($invite_id);

            if ($invite->get_user_id() != $username) return new ForbiddenError();

            else {
                $model->accept($invite);
            }
        } catch (EntryNotFound $e) {
            return new NotFoundError($e->getMessage());
        } catch (DatabaseError $e) {
            return new ServiceUnavailableError($e->getMessage());
        }
        return new EmptyResponse();
    }

    public static function decline(Base $app, array $params): Response
    {
        return new NotImplementedError();
    }
}