<?php namespace views;

use Base;

use models\errors\DatabaseError;
use models\errors\EntryAlreadyExists;
use models\errors\EntryNotFound;
use models\errors\ValidationError;
use models\user\RawUserData;
use models\user\User;
use models\user\UsersModel;

use views\responses\EmptyResponse;
use views\responses\NotFoundError;
use views\responses\NotImplementedError;
use views\responses\ResourceConflictError;
use views\responses\Response;
use views\responses\ServiceUnavailableError;
use views\responses\UnprocessableEntityError;


/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 15.03.19
 * Time: 14:11
 */
class UsersView
{
    /**
     * Accepts RawUserData using the POST method,
     * then creates an instance of User and writes it to the database.
     * If the recording is unsuccessful - throws one of the model exceptions
     * which is mapped to different Response types.
     *
     * @param Base $app - FatFree app instance
     * @return Response - any Response subclass can be returned from this route
     */
    public static function create_new_user(Base $app): Response
    {
        $model = new UsersModel($app->get("db"));

        try {
            $user_data = new RawUserData(json_decode($app->get("BODY")));
            $user = new User($user_data);
            $model->create_user($user);
        } catch (ValidationError $e) {
            return new UnprocessableEntityError($e->getMessage());
        } catch (EntryAlreadyExists $e) {
            return new ResourceConflictError($e->getMessage());
        } catch (DatabaseError $e) {
            return new ServiceUnavailableError($e->getMessage());
        }
        return new EmptyResponse();
    }

    /**
     * Accepts RawUserData using the POST method
     * If the password hash matches the hash stored in the database,
     * set session.logged to true, otherwise 404 is returned
     *
     * @param Base $app - FatFree app instance
     * @return Response - any Response subclass can be returned from this function
     */
    public static function login(Base $app): Response
    {
        $model = new UsersModel($app->get('db'));
        try {
            $login_data = new RawUserData(json_decode($app->get('BODY')));
            if ($model->check_password($login_data)) {
                $app->set('SESSION.username', $login_data->username);
                return new EmptyResponse();
            } else {
                // Login error represented by 404 error code
                // Which means "user with such login/password combination cannot be found"
                return new NotFoundError("Invalid credentials");
            }
        } catch (ValidationError $e) {
            return new UnprocessableEntityError($e->getMessage());
        } catch (EntryNotFound $e) {
            return new NotFoundError($e->getMessage());
        }
    }

    /**
     * Clears users's session
     * For an unauthenticated user, this route does not provide any special behavior (and this is not necessary)
     *
     * @param Base $app - FatFree app instance
     * @return Response - any Response subclass can be returned from this function
     */
    public static function logout(Base $app): Response
    {
        $app->clear('SESSION');
        return new EmptyResponse();
    }

    public static function get_user_info(Base $app, array $params): Response
    {
        return new NotImplementedError();
    }

    public static function update_user(Base $app, array $params): Response
    {
        return new NotImplementedError();
    }

    public static function delete_user(Base $app, array $params): Response
    {
        return new NotImplementedError();
    }
}