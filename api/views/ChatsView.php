<?php namespace views;

use Base;

use models\chat\Chat;
use models\chat\ChatsModel;
use models\chat\RawChatData;
use models\errors\DatabaseError;
use models\errors\ValidationError;
use models\message\Message;
use models\message\RawMessageData;

use views\responses\ForbiddenError;
use views\responses\NotImplementedError;
use views\responses\Response;
use views\responses\ServiceUnavailableError;
use views\responses\UnprocessableEntityError;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 15.03.19
 * Time: 21:28
 */
class ChatsView
{
    /**
     * Return list of chats for user stored in SESSION
     * Return 403 if there is no user
     *
     * @param Base $app - FatFree app instance
     * @return Response - any Response subclass can be returned from this route
     */
    public static function get_chats(Base $app): Response
    {
        $username = $app->get('SESSION.username');
        if (!$username) {
            return new ForbiddenError();
        }
        $model = new ChatsModel($app->get('db'));
        $chats = $model->get_chats($username);
        return new Response(200, $chats);
    }

    /**
     * Accepts RawChatData using the POST method,
     * then creates an instance of Chat and writes it to the database.
     * If the recording is unsuccessful - throws one of the model exceptions
     * which is mapped to different Response types.
     * If insertion was successful - returns ID of inserted chat
     *
     * @param Base $app - FatFree app instance
     * @return Response - any Response subclass can be returned from this route
     */
    public static function new_chat(Base $app): Response
    {
        $username = $app->get('SESSION.username');
        if (!$username) {
            return new ForbiddenError();
        }
        $model = new ChatsModel($app->get('db'));

        try {
            $raw_chat_data = new RawChatData(json_decode($app->get('BODY')), $username);
            $chat = new Chat($raw_chat_data);
            $id = $model->create_chat($chat);
        } catch (ValidationError $e) {
            return new UnprocessableEntityError($e->getMessage());
        } catch (DatabaseError $e) {
            return new ServiceUnavailableError($e->getMessage());
        }

        return new Response(200, array("chat_id" => $id));
    }

    public static function update_chat(Base $app, array $params): Response
    {
        return new NotImplementedError();
    }

    public static function delete_chat(Base $app, array $params): Response
    {
        return new NotImplementedError();
    }

    /**
     * Return list of messages for chat_id stored in GET params
     * Return 403 if there is no user, or if user is not a participant of this chat
     *
     * @param Base $app - FatFree app instance
     * @param array $params - List of GET parameters
     * @return Response - any Response subclass can be returned from this route
     */
    public static function get_messages(Base $app, array $params): Response
    {
        $username = $app->get('SESSION.username');
        $chat_id = $params['chat_id'];
        $model = new ChatsModel($app->get('db'));

        if (!$username or !$model->user_is_participant($chat_id, $username)) {
            return new ForbiddenError();
        }

        $messages = $model->get_messages($chat_id);
        return new Response(200, $messages);
    }

    /**
     * Accepts RawMessageData using the POST method,
     * then creates an instance of Message and writes it to the database.
     * If user is not logged or is not a participant of chat
     * If the recording is unsuccessful - throws one of the model exceptions
     * which is mapped to different Response types.
     * If insertion was successful - returns ID of inserted message
     *
     * @param Base $app - FatFree app instance
     * @param array $params - List of GET parameters
     * @return Response - any Response subclass can be returned from this route
     */
    public static function send_message(Base $app, array $params): Response
    {
        $username = $app->get('SESSION.username');
        $chat_id = $params['chat_id'];
        $model = new ChatsModel($app->get('db'));

        if (!$username or !$model->user_is_participant($chat_id, $username)) {
            return new ForbiddenError();
        }

        try {
            $message_data = new RawMessageData(json_decode($app->get('BODY')), $chat_id, $username);
            $message = new Message($message_data);
            $message_id = $model->send_message($message);
        } catch (ValidationError $e) {
            return new UnprocessableEntityError($e->getMessage());
        } catch (DatabaseError $e) {
            return new ServiceUnavailableError($e->getMessage());
        }

        return new Response(200, array("message_id" => $message_id));
    }

    public static function edit_message(Base $app, array $params): Response
    {
        return new NotImplementedError();
    }

    public static function delete_message(Base $app, array $params): Response
    {
        return new NotImplementedError();
    }
}