<?php namespace views\responses;
/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:16
 */
class UnprocessableEntityError extends ErrorResponse
{
    public function __construct(String $error_description)
    {
        parent::__construct(422, $error_description);
    }
}