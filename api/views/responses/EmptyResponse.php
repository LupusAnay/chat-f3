<?php namespace views\responses;
/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:16
 */
class EmptyResponse extends Response
{
    public function __construct()
    {
        parent::__construct(204, array());
    }
}