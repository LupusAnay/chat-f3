<?php namespace views\responses;
/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:15
 */

class Response
{
    public function __construct(int $status_code, array $data)
    {
        header('Content-Type: application/json; charset=utf-8');
        http_response_code($status_code);
        echo json_encode($data);
    }
}