<?php namespace views\responses;
/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:16
 */
class ResourceConflictError extends ErrorResponse
{
    public function __construct(String $resource_description)
    {
        parent::__construct(409, $resource_description);
    }
}