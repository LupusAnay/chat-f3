<?php namespace views\responses;
/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:16
 */
class ServiceUnavailableError extends ErrorResponse
{
    public function __construct(String $information)
    {
        parent::__construct(503, $information);
    }
}