<?php namespace views\responses;
/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:16
 */
class NotFoundError extends ErrorResponse
{
    public function __construct(String $resource_description)
    {
        parent::__construct(404, $resource_description);
    }
}