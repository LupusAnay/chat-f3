<?php namespace views\responses;
/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:17
 */
class NotImplementedError extends ErrorResponse
{
    public function __construct()
    {
        parent::__construct(501, "This route has not yet been implemented");
    }
}