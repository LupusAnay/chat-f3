<?php namespace views\responses;

class ForbiddenError extends ErrorResponse
{
    public function __construct(String $message = "You don't have enough rights to access this resource")
    {
        parent::__construct(403, $message);
    }
}

