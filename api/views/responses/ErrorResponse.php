<?php namespace views\responses;
/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:16
 */
class ErrorResponse extends Response
{
    public function __construct(int $error_code, String $error_description)
    {
        parent::__construct($error_code, array("status" => "error", "message" => $error_description));
    }
}