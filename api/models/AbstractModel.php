<?php namespace models;

use DB;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 15.03.19
 * Time: 22:28
 */

/**
 * Class AbstractModel
 * Represents Database Model (Read - Table) with Domain-Specific operations
 */
abstract class AbstractModel
{
    protected $db;

    /**
     * AbstractModel constructor.
     * Accepts instance of FatFree database connection manager
     *
     * @param \DB\SQL $db
     */
    public function __construct(DB\SQL $db)
    {
        $this->db = $db;
    }
}