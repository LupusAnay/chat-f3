<?php namespace models\message;

use models\errors\ValidationError;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:04
 */

// This may not seem obvious, but using such a scheme (Item - RawItemData)
// allows you to access the raw data of an item
// (for example, $password and $password_hash in a User Item).
// It looks implicitly, but I could not find a better solution.
//
// It may be easier to look at this solution like this:
// RawData is a check for availability, and Item is a check for validity.
class RawMessageData
{
    public $id;
    public $chat_id;
    public $text;
    public $sender;

    /**
     * RawMessageData constructor.
     * @param $message_data
     * @param String $chat_id
     * @param String $sender_username
     * @throws ValidationError
     */
    public function __construct($message_data, String $chat_id, String $sender_username)
    {
        if (!$message_data->text or !$chat_id or !$sender_username) {
            throw new ValidationError("Wrong message JSON format: 'text', 'username' or 'chat_id' is missing");
        }

        $this->sender = $sender_username;
        $this->chat_id = $chat_id;

        // Intended lack of validation for id field: it is null it'll be handled in Chat constructor
        $this->id = $message_data->id;
        $this->text = $message_data->text;
    }
}