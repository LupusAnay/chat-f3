<?php namespace models\message;

use models\AbstractItem;
use models\errors\ValidationError;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:04
 */
class Message extends AbstractItem
{
    protected $id;
    protected $chat_id;
    protected $text;
    protected $sender_username;

    /**
     * Message constructor.
     * @param RawMessageData $data
     * @throws ValidationError
     */
    public function __construct(RawMessageData $data)
    {
        $this->id = $data->id ? $data->id : uniqid();
        $this->chat_id = $data->chat_id;
        $this->sender_username = $data->sender;

        $this->set_text($data->text);
    }

    /**
     * @param String $text
     * @throws ValidationError
     */
    public function set_text(String $text)
    {
        // WARNING: I'm not sure how this rule gonna work in real production environment
        // May be it's going to trim something that it shouldn't
        $text = trim($text);
        if (strlen($text) < 1) {
            throw new ValidationError("Message text cannot be empty");
        }
        $this->text = $text;
    }

    public function as_array(): array
    {
        return array(
            "id" => $this->id,
            "chat_id" => $this->chat_id,
            "sender_username" => $this->sender_username,
            "text" => $this->text
        );
    }

    public function get_id()
    {
        return $this->id;
    }
}
