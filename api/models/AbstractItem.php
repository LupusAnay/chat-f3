<?php namespace models;
/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 15.03.19
 * Time: 22:34
 */


/**
 * Class AbstractItem
 * Represents interface for all Model's items
 */
abstract class AbstractItem
{
    /**
     * Abstract function for all elements of the model -
     * they should provide the ability to represent them in the form of an associative array
     *
     * It returns not all fields because as_array function represents Database row, not the Item itself
     * and some entities (e.g. participants or messages in Chat item) are represented in other tables
     *
     * @return array
     */
    abstract public function as_array(): array;
}