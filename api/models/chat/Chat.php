<?php namespace models\chat;

use models\AbstractItem;
use models\errors\ValidationError;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 15.03.19
 * Time: 22:26
 */

/**
 * Class Chat
 *
 * Requires an instance of RawChatData to instantiate
 */
class Chat extends AbstractItem
{
    protected $id;
    protected $name;
    protected $participants;
    protected $messages;

    /**
     * Chat constructor.
     * @param RawChatData $data
     * @throws ValidationError
     */
    public function __construct(RawChatData $data)
    {
        $this->id = $data->id ? $data->id : uniqid();
        $this->participants = $data->participants;
        $this->set_name($data->name);
    }

    /**
     * @param String $name
     * @throws ValidationError
     */
    public function set_name(String $name)
    {
        $name = trim($name);
        if (strlen($name) < 6) {
            throw new ValidationError("Chat name must be at least 6 characters long and not contain spaces");
        }
        $this->name = $name;
    }

    public function get_id(): String
    {
        return $this->id;
    }

    public function get_participants(): array
    {
        return $this->participants;
    }

    public function as_array(): array
    {
        return array(
            "id" => $this->id,
            "name" => $this->name
        );
    }
}