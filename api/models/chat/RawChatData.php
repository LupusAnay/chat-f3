<?php namespace models\chat;

use models\errors\ValidationError;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:02
 */

// This may not seem obvious, but using such a scheme (Item - RawItemData)
// allows you to access the raw data of an item
// (for example, $password and $password_hash in a User Item).
// It looks implicitly, but I could not find a better solution.
//
// It may be easier to look at this solution like this:
// RawData is a check for availability, and Item is a check for validity.

class RawChatData
{
    public $name;
    public $id;
    public $participants;
    public $messages;

    /**
     * RawChatData constructor.
     * @param $raw_data
     * @param String $creator_username
     * @throws ValidationError
     */
    public function __construct($raw_data, String $creator_username)
    {
        if (!$raw_data->name) {
            throw new ValidationError("Wrong JSON format");
        }

        $this->participants = array($creator_username);

        // Intended lack of validation for id field: it is null it'll be handled in Chat constructor
        $this->id = $raw_data->id;
        $this->name = $raw_data->name;
    }
}