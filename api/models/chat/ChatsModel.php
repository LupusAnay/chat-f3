<?php namespace models\chat;

use models\AbstractModel;
use models\errors\DatabaseError;
use models\message\Message;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 15.03.19
 * Time: 22:26
 */
class ChatsModel extends AbstractModel
{
    /**
     * Accepts a Chat instance, then creates a DB transaction
     * where inserts Chat in chats table and for each value in $participants array
     * inserts it in participants table
     *
     * @param Chat $chat
     * @return String
     * @throws DatabaseError
     */
    public function create_chat(Chat $chat): String
    {
        $this->db->begin();

        $this->db->exec(
            'INSERT INTO chats (id, name) VALUES (:id, :name)',
            $chat->as_array()
        );

        foreach ($chat->get_participants() as $participant) {
            // TODO: Find better solution to set admin for creator
            $count_of_inserted = $this->db->exec(
                'INSERT INTO participants (chat_id, user_id, is_admin) VALUES (?, ?, ?)',
                array($chat->get_id(), $participant, true)
            );

            if ($count_of_inserted != 1) {
                $this->db->rollback();
                throw new DatabaseError("Error while inserting participants");
            }
        }

        $this->db->commit();
        return $chat->get_id();
    }

    public function get_messages(String $chat_id): array
    {
        // TODO: It's better to return Message instances instead of anonymous objects
        $selected_messages = $this->db->exec(
            'SELECT * FROM messages WHERE chat_id = ?',
            $chat_id
        );
        return $selected_messages;
    }

    /**
     * @param String $chat_id
     * @param String $username
     * @return bool - is user a participant of this chat?
     */
    public function user_is_participant(String $chat_id, String $username): bool
    {

        $chats = $this->get_chats($username);

        foreach ($chats as $chat) {
            if ($chat['id'] == $chat_id) {
                return true;
            }
        }

        return false;
    }

    public function get_chats(String $username): array
    {
        // TODO: It's better to return Chat instances instead of anonymous objects
        $selected_chats = $this->db->exec(
            'SELECT chats.id FROM chats INNER JOIN participants p ON chats.id = p.chat_id WHERE p.user_id = ?',
            $username
        );
        return $selected_chats;
    }

    /**
     * @param Message $message
     * @return String - id of inserted message
     * @throws DatabaseError
     */
    public function send_message(Message $message): String
    {
        $inserted_count = $this->db->exec(
            'INSERT INTO messages (id, chat_id, sender_username, text) VALUES (:id, :chat_id, :sender_username, :text)',
            $message->as_array()
        );

        if ($inserted_count != 1) {
            throw new DatabaseError("Inserted count is not equal 1");
        }

        return $message->get_id();
    }

    public function user_is_admin(String $chat_id, String $username): bool
    {
        $result = $this->db->exec(
            'SELECT * FROM participants WHERE chat_id = ? AND user_id = ?',
            array($chat_id, $username)
        );

        if (!$result or !$result[0]['is_admin']) return false;
        else return true;
    }
}