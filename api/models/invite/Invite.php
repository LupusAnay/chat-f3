<?php namespace models\invite;
use models\AbstractItem;
use models\errors\ValidationError;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:02
 */
class Invite extends AbstractItem
{
    protected $id;
    protected $chat_id;
    protected $user_id;

    /**
     * Invite constructor.
     * @param $data
     * @throws ValidationError
     */
    public function __construct($data)
    {
        if (!$data->chat_id or !$data->user_id) {
            throw new ValidationError("Invalid invite JSON");
        }
        $this->id = $data->id ? $data->id : uniqid();
        $this->chat_id = $data->chat_id;
        $this->user_id = $data->user_id;
    }

    public function as_array(): array
    {
        return array(
            "id" => $this->id,
            "chat_id" => $this->chat_id,
            "user_id" => $this->user_id
        );
    }

    public function get_chat_id()
    {
        return $this->chat_id;
    }

    public function get_user_id()
    {
        return $this->user_id;
    }

    public function get_id()
    {
        return $this->id;
    }
}
