<?php namespace models\invite;

use models\AbstractModel;
use models\errors\DatabaseError;
use models\errors\EntryAlreadyExists;
use models\errors\EntryNotFound;
use models\errors\ValidationError;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:03
 */
class InvitesModel extends AbstractModel
{

    /**
     * @param Invite $invite
     * @return String
     * @throws EntryAlreadyExists
     * @throws DatabaseError
     */
    public function create_invite(Invite $invite): String
    {
        if ($this->invite_exists($invite)) {
            throw new EntryAlreadyExists("Such invite already exists");
        }

        $count_of_inserted_rows = $this->db->exec(
            'INSERT INTO invites (id, user_id, chat_id) VALUES (:id, :user_id, :chat_id)',
            $invite->as_array()
        );

        if ($count_of_inserted_rows != 1) {
            throw new DatabaseError("Error while creating new invite");
        }

        return $invite->get_id();
    }

    private function invite_exists(Invite $invite)
    {
        $invite = $this->db->exec(
            'SELECT * FROM invites WHERE user_id = ? AND chat_id = ?',
            array($invite->get_user_id(), $invite->get_chat_id())
        );

        return count($invite) >= 1;
    }

    /**
     * @param String $invite_id
     * @return Invite
     * @throws EntryNotFound
     * @throws DatabaseError
     */
    public function get_by_id(String $invite_id): Invite
    {
        $invite_data = $this->db->exec(
            'SELECT * FROM invites WHERE id = ?',
            $invite_id
        );
        if (!$invite_data[0]) {
            throw new EntryNotFound("Cannot find invite with id " . $invite_id);
        }

        try {
            $invite = new Invite((object)$invite_data[0]);
        } catch (ValidationError $e) {
            throw new DatabaseError("Cannot parse Invite object from database data");
        }
        return $invite;
    }

    /**
     * @param Invite $invite
     * @throws DatabaseError
     */
    public function accept(Invite $invite)
    {
        $affected_rows = $this->db->exec(
            array(
                'INSERT INTO participants (chat_id, user_id) VALUES (?, ?)',
                'DELETE FROM invites WHERE invites.id = ?'
            ),
            array(
                array($invite->get_chat_id(), $invite->get_user_id()),
                array($invite->get_id())
            )
        );
        var_dump($affected_rows);
        if ($affected_rows != 2) {
            throw new DatabaseError("Error while accepting invite");
        }
    }
}