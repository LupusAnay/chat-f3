<?php namespace models\user;
use models\AbstractItem;
use models\errors\ValidationError;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:05
 */

/**
 * Class Chat
 *
 * Requires an instance of RawUserData to instantiate
 */
class User extends AbstractItem
{
    protected $username;
    protected $password_hash;
    protected $chats;
    protected $clusters;
    protected $invitations;

    /**
     * User constructor.
     * @param $raw_user_data
     * @throws ValidationError
     */
    public function __construct(RawUserData $raw_user_data)
    {
        $this->set_username($raw_user_data->username);
        $this->set_password_hash($raw_user_data->raw_password);
    }

    public function as_array(): array
    {
        return array(
            "username" => $this->get_username(),
            "password_hash" => $this->get_password_hash()
        );
    }

    public function get_username()
    {
        return $this->username;
    }

    /**
     * @param String $username
     * @throws ValidationError
     */
    public function set_username(String $username)
    {
        $username = trim($username);
        if (strlen($username) < 6) {
            throw new ValidationError("Username must be at least 6 characters long and not contain spaces");
        }
        $this->username = $username;
    }

    public function get_password_hash()
    {
        return $this->password_hash;
    }

    public function set_password_hash(String $password)
    {
        $this->password_hash = password_hash($password, PASSWORD_DEFAULT);
    }
}
