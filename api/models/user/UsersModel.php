<?php namespace models\user;
use models\AbstractModel;
use models\errors\DatabaseError;
use models\errors\EntryAlreadyExists;
use models\errors\EntryNotFound;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 15.03.19
 * Time: 16:51
 */
class UsersModel extends AbstractModel
{
    /**
     * @param User $user
     * @throws DatabaseError
     * @throws EntryAlreadyExists
     */
    public function create_user(User $user)
    {
        if ($this->user_exists($user->get_username())) {
            throw new EntryAlreadyExists("User with username " . $user->get_username() . " already exists");
        }

        $count_of_affected_rows = $this->db->exec(
            'INSERT INTO users (username, password) VALUES (:username, :password_hash)',
            $user->as_array()
        );

        if ($count_of_affected_rows != 1) {
            throw new DatabaseError("Error while creating new user");
        }
    }

    private function user_exists(String $username): bool
    {
        $user = $this->db->exec(
            'SELECT * FROM users WHERE username=?',
            $username
        );

        return count($user) == 1;
    }

    /**
     * @param RawUserData $login_data
     * @return bool
     * @throws EntryNotFound
     */
    public function check_password(RawUserData $login_data): bool
    {
        $selected_user = $this->db->exec(
            'SELECT * FROM users WHERE username=?',
            $login_data->username
        );

        if (!$selected_user) {
            throw new EntryNotFound("User with username " . $login_data->username . " does not exists");
        }

        return password_verify($login_data->raw_password, $selected_user[0]['password']);
    }
}
