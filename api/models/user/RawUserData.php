<?php namespace models\user;
use models\errors\ValidationError;

/**
 * Created by PhpStorm.
 * User: lupusanay
 * Date: 16.03.19
 * Time: 20:06
 */

// This may not seem obvious, but using such a scheme (Item - RawItemData)
// allows you to access the raw data of an item
// (for example, $password and $password_hash in a User Item).
// It looks implicitly, but I could not find a better solution.
//
// It may be easier to look at this solution like this:
// RawData is a check for availability, and Item is a check for validity.
class RawUserData
{
    public $username;
    public $raw_password;
    public $chats;
    public $clusters;
    public $invitations;

    /**
     * RawUserData constructor.
     * @param $data
     * @throws ValidationError
     */
    public function __construct($data)
    {
        if ($data->username == null or $data->password == null) {
            throw new ValidationError("Wrong JSON format");
        }
        $this->username = $data->username;
        $this->raw_password = $data->password;
    }
}
